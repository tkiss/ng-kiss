import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { User, Profile } from '../core';
import { concatMap ,  tap } from 'rxjs/operators';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile.component.html'
})
export class ProfileComponent {
  constructor(
    private route: ActivatedRoute,
  ) { }

  profile: Profile;
  currentUser: User;
  isUser: boolean;

  onToggleFollowing(following: boolean) {
    this.profile.following = following;
  }

}
