import {CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, NgModule} from '@angular/core';
import { RouterModule } from '@angular/router';

import { ArticleComponent } from './article.component';
import { ArticleCommentComponent } from './article-comment.component';
import { MarkdownPipe } from './markdown.pipe';
import { SharedModule } from '../shared';
import { ArticleRoutingModule } from './article-routing.module';
import {NgxSpinnerModule} from "ngx-spinner";

@NgModule({
    imports: [
        SharedModule,
        ArticleRoutingModule,
        NgxSpinnerModule
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    ArticleComponent,
    ArticleCommentComponent,
    MarkdownPipe
  ],

  providers: [
  ]
})
export class ArticleModule {}
